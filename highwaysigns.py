#!/usr/bin/env python
# coding: utf-8
# import statements
import requests
import pandas as pd
import html
from bs4 import BeautifulSoup
from w3lib.html import replace_entities

# constants
url = "https://chartexp1.sha.maryland.gov/CHARTExportClientService/getDMSMapDataJSON.do"

# getting response
response = requests.request("GET", url).json()

# converting to dataframe
df = pd.DataFrame(response['data'])
df.msgHTML = df.msgHTML.apply(html.unescape)

print(df.dtypes)

# saving dataframe to csv
df.to_csv('output/response_python.csv')
